# MinAFDC <small>Minimizador de Autômato Finito Determinístico Completo</small>

## README

## Integrantes

- Breno Gomes
- Felipe Ferreira
- Lorena Tavares

### Modo de usar

Chame o arquivo executável `main.py` da seguinte forma:

> `$ ./main.py  <descricao-afd> <arquivo-saida-tabela> <arquivo-saida-afd-minimizado>`

Onde o `<descricao-afd>` deve ser um arquivo de texto com a descrição do AFD **completo** com o seguinto formato

```
(
	{<estados>},
	{<alfabeto>},
	{
		<transições>
	},
	<estado-inicial>,
	{<estados-finais>}
)
```

Onde:
- `<estados>` são os estados do AFD separados por vírgula, formado por pelo menos uma letra se guida por pelo menos um dígito. Formato em _RegEx_: `\w+\d+`.
- `<alfabeto>` são os símbolos do alfabeto formado por _uma única_ letra. Formato em _RegEx_: `\w`.
- `<transições>` são as funções de transição deterministica do AFD separadas por vírgula e uma em cada linha com o seguinto formato em _RegEx_: `(\w+\d+,\w->\w+\d+)`
- `<estado-inicial>` é o estado inicial do AFD. Fomato _RegEx_: `\w+\d+`.
- `<estados-finais>` são os estados finais do AFD com o mesmo formato de `<estados>`.

### Observação

Os arquivos `class_auto.py` e `functions.py` devem estar no mesmo diretório que o arquivo `main.py` para que o programa funcione.
