#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import logging
from class_auto import Auto


def junta_transicoes(trans):
    t = []

    for i in trans:
        li = [(a, b, c) for (a, b, c) in t if i[0] == a and i[2] == c]

        if not len(li):
            t.append(i)

        else:
            for j in li:
                _j = list(t[t.index(j)])
                _j[1] = t[t.index(j)][1] + ' ' + i[1]
                t[t.index(j)] = tuple(_j)

    return t


def gen_dot(auto):
    s = "digraph \"graph\" {\n\trankdir=LR\n\tnode [shape=point]\n\tstart\n"

    s += "\tnode [shape=doublecircle]\n"
    for i in auto.finais:
        s += '\t{}\n'.format(i)

    nao_finais = [x for x in auto.estados if x not in auto.finais]

    s += '\tnode [shape=circle]\n'
    for i in nao_finais:
        s += '\t{}\n'.format(i)

    s += '\tstart -> {}\n'.format(auto.inicial)

    for i in junta_transicoes(auto.trans):
        s += '\t{} -> {} [label="{}"]\n'.format(i[0], i[2], i[1])

    s += '}'
    return s


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    logger = logging.getLogger(__name__)
    logger.info('Minimiza engage')

    if(len(sys.argv) != 4):
        print("Modo de uso")
        print("./main.py <automato-base> <tabela-de-saida> <automato-de-saida>")
        sys.exit()

    # Criar automato apartir do arquivo
    auto = Auto(sys.argv[1])
    auto.minimiza()
    print(auto)
    print(gen_dot(auto), file=open(sys.argv[1] + '.dot', 'w'))
